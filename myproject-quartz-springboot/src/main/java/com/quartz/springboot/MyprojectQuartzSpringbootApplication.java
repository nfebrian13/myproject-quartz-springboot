package com.quartz.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyprojectQuartzSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyprojectQuartzSpringbootApplication.class, args);
	}

}
