package com.quartz.springboot.dto;

public class SchedulerRequest {

	private String jobName;
	private String jobScheduleTime;
	private String cronExpression;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobScheduleTime() {
		return jobScheduleTime;
	}

	public void setJobScheduleTime(String jobScheduleTime) {
		this.jobScheduleTime = jobScheduleTime;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

}
